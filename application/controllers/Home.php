<?php 

class Home extends CI_Controller
{
  public function index()
  {
    $data['title'] = 'Himatif';
    $this->load->view('homepage/index', $data);
  }
}