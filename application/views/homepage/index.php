<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?= $title; ?></title>
  <link rel="stylesheet" href="<?= base_url('assets/plugin/bootstrap/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/app.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/app-homepage.min.css') ?>">
</head>
<body>


<section class="homepage">
  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="<?= base_url('assets/img/logo/logo.png') ?>" alt="" width="30" height="30">
        <span class="tx-secondary tx-weight-4">HIMATIF</span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">ABOUT</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              DIVISI
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">BPH</a>
              <a class="dropdown-item" href="#">MEDIATEK</a>
              <a class="dropdown-item" href="#">LITBANG</a>
              <a class="dropdown-item" href="#">PSDM</a>
              <a class="dropdown-item" href="#">HUMAS</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">PENGURUS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">PROKER</a>
          </li>
          <li class="nav-item focus-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              PUSAT BANTUAN
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">HIMATIF BLOG</a>
              <a class="dropdown-item" href="#">SOCIAL MEDIA</a>
              <a class="dropdown-item" href="#">EMAIL</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- end of navbar -->

  <!-- home section -->
  <div class="homepage-home">
    <img src="<?= base_url('assets/img/picture/bg.png') ?>" alt="" class="img-bg">
    <div class="container tx-secondary">

      <div class="row">

        <div class="col-lg-5 col-12 text-lg-left text-center mt-5">
          <div class="homepage-home-info">
            <div class="row">
              <div class="col-lg-4"> 
                <img src="<?= base_url('assets/img/logo/logo.png') ?>" alt="">
              </div>
              <div class="col-lg-8 py-3 ">
                <h1 class="">HIMATIF</h1>
                <p class="">Himpunan Mahasiswa Teknologi Informasi</p>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mt-4 text-center">
                <p class="text-center">"Muda tidak Menyerah"</p>
                <a href="" class="btn btn-gradient-primary">Click For More</a>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="col-12">
          <div class="homepage-home-address">
            <p class="text-center">
              <span class="d-md-inline d-block">Himpunan Mahasiswa Teknologi Informasi</span>
              <span class="d-md-inline d-block"> - Ilmu Komputer - </span> 
              Universitas Jember
            </p>
            <hr class="divider">
            <p class="text-center tx-weight-3">OFFICIAL WEBSITE</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- home section -->

  <!-- advantages -->
  <div class="container tx-secondary">
    <div class="row justify-content-center">
      <div class="col-xl-8 col-md-12 text-center">
        <div class="homepage-home-advance p-md-5 px-4 py-5 bg-white">
        <div class="row">
          <div class="col-4">
            <h3 class="tx-weight-6">5</h3>
            <hr class="divider-sm my-2">
            <span>Divisi</span>
          </div>
          <div class="col-4">
            <h3 class="tx-weight-6">35</h3>
            <hr class="divider-sm my-2">
            <span>Pengurus</span>
          </div>
          <div class="col-4">
            <h3 class="tx-weight-6">17</h3>
            <hr class="divider-sm my-2">
            <span>Proker</span>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end of advantages -->


  <!-- about -->

  <div class="homepage-about tx-secondary">
    <div class="container">
      <div class="title-row text-right mr-md-5">
        <h1>ABOUT</h1>
        <hr>
      </div>

      <div class="row">
        <div class="col-12 mt-xl-5 mt-2 px-md-5 p-3">
          <p class="px-md-5 px-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore veniam deleniti necessitatibus dolor ab dolorem amet, voluptatibus alias minima iste beatae harum, hic, fugit maiores est vitae cumque id neque officia sapiente quos a minus! Doloribus fuga sed tenetur totam quo, quidem laboriosam repellendus error. Voluptatibus, nulla sunt perferendis adipisci aliquid quasi vitae, sed quo enim nobis iusto dignissimos possimus culpa quidem voluptatum alias distinctio. Soluta natus alias ea illo pariatur consequuntur ex perspiciatis quae architecto ut inventore error veniam provident maxime sit vel amet est, totam cumque in? Earum repellat neque itaque impedit at nam blanditiis aperiam ad sapiente!</p>
        </div>
      </div>

    </div>
  </div>

  <!-- end of about -->

  <!-- division -->

  <div class="homepage-division tx-secondary">
    <div class="container">
      <div class="title-row text-left ml-md-5">
        <h1>DIVISI</h1>
        <hr>
      </div>

      <div class="row">
        <!-- box selection -->
        <div class="col-xl-6 col-md-6 col-12">
          <div class="row">
            <div class="col-xl-6 col-md-6 col-12 pl-md-5 d-flex justify-content-between d-md-block">
              <div class="box box-white">
                <div class="decoration decoration-tl"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
              <div class="box box-white">
                <div class="decoration decoration-cl"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
              <div class="box box-white">
                <div class="decoration decoration-bl"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
              <div class="box box-white d-md-none">
                <div class="decoration decoration-tr"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
              <div class="box box-white d-md-none">
                <div class="decoration decoration-br"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
            </div>
            <div class="col-xl-6 col-md-6 col-12 mt-md-5 pt-xl-5 d-none d-md-block">
              <div class="box box-white">
                <div class="decoration decoration-tr"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
              <div class="box box-white">
                <div class="decoration decoration-br"></div>
                <span class="icon"><img src="<?= base_url('assets/img/icons/humas_primary.png') ?>" alt=""></span>
                <span class="text">BPH</span>
              </div>
            </div>
          </div>
        </div>
        <!-- end of box selection -->

        <!-- description of box -->
        <div class="col-xl-6 col-md-6 col-12 pt-xl-5">
          <div class="box-description float-right">
            <h1 class="tx-weight-6">BPH</h1>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Fuga expedita labore suscipit minus veniam, consectetur eveniet beatae excepturi itaque aspernatur, eligendi, quidem blanditiis tempore? Excepturi repellat error fugit, placeat inventore neque temporibus ex assumenda at sint dolorum maiores sit ab totam esse in labore tenetur consequatur iste itaque quia nulla nihil laborum? Ipsam optio exercitationem eum repellendus impedit officia quibusdam iste dolorem maiores, inventore a voluptatum vel quae ad modi. Fugiat quaerat aliquam, minima officiis repellendus dolorum expedita ea, obcaecati veniam dolores iure deserunt explicabo nobis magni nisi consequuntur velit? Saepe, fugiat natus. Molestiae rem, nulla voluptatum dolor maiores officiis.</p>
            <p class="read-more" ><a href="#" class="btn btn-white button px-5 py-2 tx-12">Read More</a></p>
          </div>
        </div>
        <!-- end of description of box -->

      </div>

    </div>
  </div>

  <!-- end of division -->


  <!-- teams -->
  <div class="homepage-teams tx-secondary">
    <div class="container">
      <div class="title-row text-center ml-md-5">
        <h1>PENGURUS</h1>
        <hr>
      </div>

      <div class="row">
        <div class="col-xl-12">
          <ul class="nav nav-pills" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="bph-tab" data-toggle="tab" href="#bph" role="tab" aria-controls="bph" aria-selected="true">BPH</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="mediatek-tab" data-toggle="tab" href="#mediatek" role="tab" aria-controls="mediatek" aria-selected="false">MEDIATEK</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="litbang-tab" data-toggle="tab" href="#litbang" role="tab" aria-controls="litbang" aria-selected="false">LITBANG</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="psdm-tab" data-toggle="tab" href="#psdm" role="tab" aria-controls="psdm" aria-selected="false">PSDM</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="humas-tab" data-toggle="tab" href="#humas" role="tab" aria-controls="humas" aria-selected="false">HUMAS</a>
            </li>
          </ul>
          <div class="tab-content p-5" id="myTabContent">
            <!-- tab-pane BPH 1 -->
            <div class="tab-pane fade show active" id="bph" role="tabpanel" aria-labelledby="bph-tab">
            <!-- carousel -->
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
               
                <!-- carousel inner -->
                  <div class="carousel-inner"> 
                    <div class="carousel-item active">
                      <div class="row tx-secondary justify-content-center">
                        
                        <div class="col-xl-4 col-md-5 col-6 ml-xl-5">
                          <div class="img-decorated ">
                            <img src="<?= base_url('assets/img/decoration/decoration_img.png') ?>" alt="" class="decoration d-xl-block d-none">
                            <div class="img-inside">
                              <img src="<?= base_url('assets/img/picture/bayu.png') ?>" alt="" class="img-fluid">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-5 col-md-7 col-6 pl-xl-4 text-left">
                          <div class="description-teams mt-4">
                            <h1>Bayu Satrio</h1>
                            <h4 class="division">Ketua Umum - BPH</h4>
                            <P>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate assumenda itaque facere voluptatum natus neque eveniet delectus. Dolorum, vero aliquid.</P>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  <!-- end of tab-pane BPH 1 -->
                </div>
                <!-- end of carousel inner -->

                <!-- carousel control -->
                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                  <img src="<?= base_url('assets/img/icons/rightSlide.png') ?>" alt="">
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                  <img src="<?= base_url('assets/img/icons/leftSlide.png') ?>" alt="">
                  <span class="sr-only">Next</span>
                </a>
                <!-- end of carousel control -->
            </div>
            <!-- end of carousel -->

            <div class="tab-pane fade" id="mediatek" role="tabpanel" aria-labelledby="mediatek-tab">MEDIATEK</div>
            <div class="tab-pane fade" id="litbang" role="tabpanel" aria-labelledby="litbang-tab">LITBANG</div>
            <div class="tab-pane fade" id="psdm" role="tabpanel" aria-labelledby="psdm-tab">PSDM</div>
            <div class="tab-pane fade" id="humas" role="tabpanel" aria-labelledby="humas-tab">HUMAS</div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- end of teams -->

  <!-- projects -->

  <div class="homepage-project tx-secondary">
    <div class="container">
      <div class="title-row text-right ml-md-5 mb-5">
        <h1>PROKER</h1>
        <hr>
      </div>

      <div class="row">
        <!-- col artobard -->
        <div class="col-md-6">
          <img src="<?= base_url('assets/img/flat_design/proker-2.png') ?>" alt="" class="img-fluid mb-md-0 mb-5">
        </div>
        <!-- end of col artboard -->

        <!-- col project -->
        <div class="col-md-6">
          <div class="card-group">
            <div class="card-small">
              <img src="<?= base_url('assets/img/icons/proker.png') ?>" alt="" class="img-card">
              <span class="title-card">Technology Innovative Challenge</span>
            </div>
            <div class="card-small">
              <img src="<?= base_url('assets/img/icons/proker.png') ?>" alt="" class="img-card">
              <span class="title-card">Technology Innovative Challenge</span>
            </div>
            <div class="card-small">
              <img src="<?= base_url('assets/img/icons/proker.png') ?>" alt="" class="img-card">
              <span class="title-card">Technology Innovative Challenge</span>
            </div>
            <div class="card-small">
              <img src="<?= base_url('assets/img/icons/proker.png') ?>" alt="" class="img-card">
              <span class="title-card">Technology Innovative Challenge</span>
            </div>
            <div class="card-small">
              <img src="<?= base_url('assets/img/icons/proker.png') ?>" alt="" class="img-card">
              <span class="title-card">Technology Innovative Challenge</span>
            </div>
          </div>
        </div>
        <!-- end of col project -->

      </div>

    </div>
  </div>

  <!-- end of projects -->

  <!-- detail projects -->
  <div class="homepage-detail-project tx-secondary">
    <div class="container">
      <div class="title-row text-center ml-md-5">
        <h1>DETAIL PROKER</h1>
        <hr>
      </div>

      <div class="row justify-content-end">
        <div class="col-md-6 col-lg-4 ">
          <div class="input-group mb-2">
            <select class="custom-select" id="inputGroupSelect01">
              <option selected>Proker Lainnya...</option>
              <option value="1">ITeC</option>
              <option value="2">Character Organization Development</option>
              <option value="3">Technology Innovative Challenge</option>
            </select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">

          <div class="img-lg-decorated">
            <div class="img-inside">
              <img src="<?= base_url('assets/img/picture/examplePost.png') ?>" alt="">
            </div>
          </div>

        </div>
      </div>

      <div class="row">
        <div class="col-12 pr-xl-5">
          <div class="title-project">
            <h1>Technology Innovative Challenge</h1>
            <span>17 Desember 2019</span>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12 px-md-5 ">
          <p class="long-description mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur ab quisquam libero dolorum, veritatis quibusdam ullam aliquid recusandae rem placeat earum tempore suscipit perspiciatis reiciendis porro officiis. Omnis similique, repellendus, quaerat ipsum, velit veniam exercitationem aspernatur voluptatum cupiditate illum eveniet nihil ullam eum labore sed impedit quidem veritatis illo eligendi voluptas. Nostrum, omnis provident maxime totam harum cumque veritatis impedit iure vero labore et necessitatibus, doloribus reiciendis quis! Consectetur minima et, rerum dolorum quaerat, laudantium commodi excepturi, assumenda mollitia error debitis? Ducimus non voluptatibus distinctio hic facilis officia voluptate tempore exercitationem dolores pariatur eius maiores, aliquam earum consequatur quidem ut.</p>
        </div>
      </div>

    </div>
    

  </div>
  <!-- end of detail projects -->
  
  <!-- footer -->
  <footer class="homepage-footer">
    <div class="container">
      <div class="row tx-secondary">
        <div class="col-xl-5">
          <img src="<?= base_url('assets/img/logo/logo.png') ?>" alt="" class="logo-footer">
        </div>
        <div class="col-xl-7 text-center">
          <h1>HIMATIF</h1>
        </div>
      </div>
    </div>
  </footer>
  <!-- end of footer -->

  
</section>



<script src="<?= base_url('assets/plugin/jquery/jquery-3.3.1.js') ?>"></script>
<script src="<?= base_url('assets/plugin/bootstrap/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/homepage.js') ?>"></script>
</body>
</html>